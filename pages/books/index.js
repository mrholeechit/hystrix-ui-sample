import React, { PureComponent } from "react";
import styled from "styled-components";
import fetch from "isomorphic-unfetch";

const Button = styled.button`
  margin-top: 10px;
`;

class BookPage extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { interval: 500, requestCount: 0, books: [] };
  }

  onChange(inputValue) {
    this.setState({ value: inputValue });
  }

  onRequest() {
    // this.timerID = setInterval(() => this.doRequest(), this.state.interval);
    this.doRequest();
  }

  doRequest() {
    this.setState({ requestCount: this.state.requestCount + 1 });
    return fetch("http://localhost:9090/data/v1/books")
      .then(res => res.json())
      .then(data => {
        this.setState({ books: [...this.state.books, data[0]] });
      });
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  render() {
    return (
      <div>
        <label>Request Count - {this.state.requestCount}</label>
        <br />
        <Button onClick={() => this.onRequest()}>Send request</Button>
        <ul>
          {this.state.books.map(book => (
            <li id={this.state.requestCount}>
              <p>{book.title}</p>
              <p>{book.author}</p>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default BookPage;
